package minesweeper.solver;

import java.util.ArrayList;
import java.util.List;

import minesweeper.core.Board;
import minesweeper.core.Square;

/**
 * @author Martín Tomás Juran
 * @version 1.0.0 Mar 30, 2019
 */
public class Solver {
	public static final int STUCK = 3;
	public static final int WON = 2;
	public static final int PLAYING = 1;
	public static final int LOST = 0;

	private Board board;
	// Border contains all squares that are revealed but not solved (i.e. there are
	// still adjacent squares to reveal or flag)
	private List<Square> border;
	private int state;
	private List<Board> forks;

	public Solver(Board board) {
		this.board = board;
		this.border = new ArrayList<Square>();
		this.state = PLAYING;
		firstMove();
	}

	public int getState() {
		return state;
	}

	private void updateBorder(Square square) {
		// Square was revealed or flagged, so adjacent squares in the border could be in
		// solved state
		List<Square> adjacents = board.getAdjacent(square.getX(), square.getY());
		for (Square adjacent : adjacents) {
			if (border.contains(adjacent)) {
				if (isSolved(adjacent)) {
					border.remove(adjacent);
					checkVictory();
				}
			}
		}
	}
	
	private boolean checkVictory() {
		boolean victory = false;
		if (border.isEmpty()) {
			boolean b = true;
			Square[][] boardSquares = board.getSquares();
			int i = 0;
			while (b && (i < boardSquares.length)) {
				int k = 0;
				Square[] row = boardSquares[i];
				while (b && (k < row.length)) {
					if (row[k].hasMine() && !row[k].isFlagged())
						b = false;
					k++;
				}
				i++;
			}
			victory = b;
		}
		if (victory) {
			state = WON;
			board.revealAll();
			System.out.println("You won! (actually, the Solver did, but hooray!)");
		}
		return victory;
	}

	private boolean isSolved(Square square) {
		// A square is solved if all adjacent squares are revealed or flagged
		boolean solved = true;
		int i = 0;
		List<Square> adjacents = board.getAdjacent(square.getX(), square.getY());
		while (solved && i < adjacents.size()) {
			Square adjacent = adjacents.get(i);
			if (!adjacent.isRevealed() && !adjacent.isFlagged())
				solved = false;
			i++;
		}
		return solved;
	}

	private void firstMove() {
		// square[size-1][size-1] can never have a mine. See Board.createBoard()
		int x = board.getSize() - 1;
		int y = x;
		revealOne(board.getSquare(x, y));
	}

	public void revealOne(Square square) {
		int x = square.getX();
		int y = square.getY();
		Square revealed = board.getSquare(x, y);
		if (revealed != null && !revealed.isRevealed()) {
			board.reveal(x, y);
			if (revealed.hasMine()) {
				state = LOST; // Tough luck :(
				board.revealAll();
				System.out.println("Lost while trying square " + printSquare(revealed));
			} else {
				System.out.println("Revealed square " + printSquare(revealed));
				if (revealed.getValue() == 0) {
					revealAdjacent(revealed);
				} else {
					if (!isSolved(revealed)) {
						border.add(revealed);
					}
				}
				updateBorder(revealed);
			}
		}
	}

	private void revealAdjacent(Square square) {
		for (Square adjacent : board.getAdjacent(square.getX(), square.getY())) {
			if (!adjacent.isFlagged())
				revealOne(adjacent);
		}
	}

	private void flagOne(Square mine) {
		if (!mine.isFlagged()) {
			int x = mine.getX();
			int y = mine.getY();
			board.flag(x, y, true);
			System.out.println("Flagged square " + printSquare(mine));
			updateBorder(mine);
		}
	}

	public void nextMove() {
		switch (state) {
		case WON:
			System.out.println("You won! Try loading another Board.");
			break;
		case LOST:
			System.out.println("You lost! Try loading another Board.");
			break;
		case STUCK:
			// Flag the highest mine probability square or reveal the lowest mine
			// probability square
			System.out.println("Solver is stuck without heuristics.");
			// reveal();
			break;
		case PLAYING:
			if (!findLogicalSolution()) {
				state = STUCK;
				System.out.println(
						"Logical deduction not possible. Heuristics needed to calculate biggest win percentage square...");
				// We reached a state where there is no logical solution;
				// therefore, try to minimize the chance to lose by looking for all possible
				// board configurations
				// findAllPossibleBoardConfigurations();
			}
			break;
		default:
			System.out.println("The Solver is in a mystical (and unknown) state.");
		}
	}

	private boolean findLogicalSolution() {
		// Find next step without using probability
		// Because border is sorted by number of adjacent squares revealed, a solution
		// is found quickly
		for (Square square : border) {
			if (allAdjacentMinesFlagged(board, square)) {
				// All mines surrounding the square are flagged, so a solution was found
				revealAdjacent(square);
				System.out.println("All squares adjacent to " + printSquare(square) + " revealed");
				return true;
			}
			if (flagOnlyPossibleSquares(square)) {
				System.out.println("All mines adjacent to " + printSquare(square) + " flagged");
				// Mine can only be in certain position(s), so flag them
				return true;
			}
		}
		return false;
	}

	private boolean allAdjacentMinesFlagged(Board board, Square square) {
		// Search if all adjacent mines are flagged
		if (square != null) {
			List<Square> adjacents = board.getAdjacent(square.getX(), square.getY());
			int flagged = 0;
			for (Square adjacent : adjacents) {
				if (adjacent.isFlagged())
					flagged++;
			}
			if (flagged == square.getValue())
				return true;
		}
		return false;
	}

	private boolean flagOnlyPossibleSquares(Square square) {
		// Flag all adjacent squares if there is no other place for mines
		List<Square> notRevealed = board.getAdjacent(square.getX(), square.getY());
		for (Square adjacent : board.getAdjacent(square.getX(), square.getY())) {
			if (adjacent.isRevealed())
				notRevealed.remove(adjacent);
		}
		if (notRevealed.size() == square.getValue()) {
			// The only not revealed adjacent square(s) MUST have a mine
			for (Square mine : notRevealed) {
				flagOne(mine);
			}
			return true;
		}
		return false;
	}

	private void findAllPossibleBoardConfigurations() {
		forks = new ArrayList<Board>();
		findPossibleBoardConfiguration(border, board);
		for (Board fork : forks) {
			System.out.println(fork.toString());
		}
	}

	private void findPossibleBoardConfiguration(List<Square> squaresToCheck, Board board) {
		if (squaresToCheck == null || squaresToCheck.isEmpty()) {
			// Add board to possible board configurations
			forks.add(board);
			return;
		}

		for (Square square : squaresToCheck) {

			if (allAdjacentMinesFlagged(board, square)) {
				// This square is solved, continue with the others
				// Create a copy of squaresToCheck
				List<Square> squaresToCheckModified = new ArrayList<Square>();
				for (Square square2 : squaresToCheck) {
					// Check all boards without the square in question
					if (!square2.equals(square))
						squaresToCheckModified.add(square2.clone());
				}
				findPossibleBoardConfiguration(squaresToCheckModified, board);

			} else {
				// Create a board with a new mine
				List<Square> adjacents = board.getAdjacent(square.getX(), square.getY());
				for (Square adjacent : adjacents) {
					// Create a board for every combination of mines in not revealed squares
					if (!adjacent.isRevealed() && !adjacent.isFlagged()) {
						// Possible mine place
						Board boardModified = board.clone();
						boardModified.flag(adjacent.getX(), adjacent.getY(), true);
						if (checkPossible(boardModified)) {
							findPossibleBoardConfiguration(squaresToCheck, boardModified);
						}
					}
				}
			}
		}
	}

	private boolean checkPossible(Board board) {
		boolean possible = true;
		for (Square square : border) {
			possible = checkOne(board, square);
		}
		return possible;
	}

	private boolean checkOne(Board board, Square square) {
		boolean possible = true;
		int adjacentFlagged = 0;
		for (Square adjacent : board.getAdjacent(square.getX(), square.getY())) {
			if (adjacent.isFlagged())
				adjacentFlagged++;
		}
		if (adjacentFlagged > square.getValue())
			possible = false;
		return possible;
	}

	private void reveal() {
		List<Square> candidates = new ArrayList<Square>();
		for (Square square : border) {
			for (Square adjacent : board.getAdjacent(square.getX(), square.getY())) {
				if (!adjacent.isRevealed() && !adjacent.isFlagged() && !candidates.contains(adjacent)) {
					candidates.add(adjacent);
				}
			}
		}

		float mineChance = 2;
		Square selected;
		for (Square candidate : candidates) {
			int mines = 0;
			int clears = 0;
			for (Board fork : forks) {
				Square square = fork.getSquare(candidate.getX(), candidate.getY());
				if (square.isFlagged())
					mines++;
			}
			float mineChanceCandidate = mines / forks.size();
		}
	}

	public String printSquare(Square square) {
		return "[" + square.getX() + ", " + square.getY() + "] (" + square.toString() + ")";
	}
}