package minesweeper;

import java.util.Scanner;

import minesweeper.core.Board;
import minesweeper.solver.Solver;

/**
 * @author Martín Tomás Juran
 * @version 1.0.0 Mar 30, 2019
 */
public class Main {
	public static final int BOARD_SIZE = 10;
	public static final int MINES = 5;

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		Board b = new Board(BOARD_SIZE, MINES);
		Solver s = new Solver(b);
		System.out.println("\n" + b.toString());
		System.out.println("\nPress Enter to continue...");
		sc.nextLine();
		while (true) {
			s.nextMove();
			System.out.println("\n" + b.toString());
			if (s.getState() != Solver.PLAYING) {
				System.out.println("Press R to restart");
			}
			String input = sc.nextLine();
			if (s.getState() != Solver.PLAYING) {
				System.out.println();
				if (input.contains("r") || input.contains("R")) {
					b = new Board(BOARD_SIZE, MINES);
					System.out.println("Generated new Board");
					s = new Solver(b);
					System.out.println("\n" + b.toString());
				}
			}
		}
	}

}
