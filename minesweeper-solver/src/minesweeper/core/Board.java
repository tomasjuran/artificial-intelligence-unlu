package minesweeper.core;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Martín Tomás Juran
 * @version 1.0.0 Mar 30, 2019
 */
public class Board implements Cloneable {

	private final int size;
	private final int mines;
	private Square[][] squares;

	public Board(Board board) {
		this.size = board.getSize();
		this.mines = board.getMines();
		this.squares = new Square[size][];
		Square[][] squaresToCopy = board.getSquares();
		for (int i = 0; i < size; i++) {
			Square[] row = new Square[size];
			for (int k = 0; k < size; k++) {
				row[k] = squaresToCopy[i][k].clone();
			}
			squares[i] = row;
		}
	}

	public Board(int size, int mines) {
		this.size = size;
		this.mines = mines;
		createBoard(mines);
	}

	public int getSize() {
		return size;
	}

	public int getMines() {
		return mines;
	}

	public Square[][] getSquares() {
		return squares;
	}

	public Square getSquare(int x, int y) {
		if (checkInbound(x, y))
			return squares[y][x];
		return null;
	}

	public List<Square> getAdjacent(int x, int y) {
		List<Square> adjacents = new ArrayList<Square>();
		for (int i = y - 1; i <= y + 1; i++) {
			for (int k = x - 1; k <= x + 1; k++) {
				if (checkInbound(k, i)) {
					adjacents.add(squares[i][k]);
				}
			}
		}
		adjacents.remove(squares[y][x]);
		return adjacents;
	}

	public boolean checkInbound(int x, int y) {
		if (y < 0 || y >= size || x < 0 || x >= size)
			return false;
		return true;
	}

	public void createBoard(int mines) {
		squares = new Square[size][];
		for (int i = 0; i < size; i++) {
			Square[] row = new Square[size];
			for (int k = 0; k < size; k++) {
				row[k] = new Square(k, i);
			}
			squares[i] = row;
		}
		// Set mines
		for (int i = 0; i < mines; i++) {
			int x;
			int y;
			do {
				// square[size-1][size-1] can never have a bomb
				x = (int) (Math.random() * (size - 1));
				y = (int) (Math.random() * (size - 1));
			} while (squares[y][x].hasMine());
			squares[y][x].setMine(true);
		}
		// Set values
		for (int i = 0; i < size; i++) {
			for (int k = 0; k < size; k++) {
				squares[i][k].setValue(checkAdjacentMines(i, k));
			}
		}
	}

	private int checkAdjacentMines(int y, int x) {
		int value = 0;
		List<Square> adjacents = getAdjacent(x, y);
		for (Square adjacent : adjacents) {
			if (adjacent.hasMine())
				value++;
		}
		return value;
	}

	public void flag(int x, int y, boolean b) {
		if (checkInbound(x, y)) {
			squares[y][x].setFlagged(b);
		}
	}

	public void reveal(int x, int y) {
		if (checkInbound(x, y) && !squares[y][x].isRevealed()) {
			squares[y][x].setRevealed(true);
		}
	}

	public void revealAll() {
		for (Square[] row : squares) {
			for (Square square : row) {
				square.setRevealed(true);
			}
		}
	}

	@Override
	public String toString() {
		String result = "";
		for (Square[] row : squares) {
			for (Square square : row) {
				result += square.toString();
			}
			result += "\n";
		}
		return result;
	}

	@Override
	public Board clone() {
		return new Board(this);
	}
}
