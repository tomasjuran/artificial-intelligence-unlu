package minesweeper.core;

/**
 * @author Martín Tomás Juran
 * @version 1.0.0 Mar 30, 2019
 */
public class Square implements Cloneable {
	protected boolean mine = false;
	protected boolean revealed = false;
	protected boolean flagged = false;
	protected int value;

	protected final int x;
	protected final int y;
	
	public Square(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	public boolean hasMine() {
		return mine;
	}

	public void setMine(boolean mine) {
		this.mine = mine;
	}

	public boolean isRevealed() {
		return revealed;
	}

	public void setRevealed(boolean revealed) {
		this.revealed = revealed;
	}

	public boolean isFlagged() {
		return flagged;
	}

	public void setFlagged(boolean flagged) {
		this.flagged = flagged;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	@Override
	public String toString() {
		if (revealed) {
			if (mine)
				return "*";
			if (value == 0)
				return ".";
			return String.valueOf(value);
		} else {
			if (flagged)
				return "⚐";
			return "-";
		}
	}

	@Override
	public Square clone() {
		try {
			return (Square) super.clone();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
		return null;
	}
	
}